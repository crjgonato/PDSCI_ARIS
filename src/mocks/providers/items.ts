import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Patient 1",
    "profilePic": "assets/img/speakers/avatar.jpg",
    "about": "Im a Patient 1.",
  };


  constructor(public http: Http) {
    let items = [
      {
        "name": "Patient 1",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 1."
      },
      {
        "name": "Patient 2",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 2."
      },
      {
        "name": "Patient 3",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 3."
      },
      {
        "name": "Patient 4",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 4."
      },
      {
        "name": "Patient 5",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 5."
      },
      {
        "name": "Patient 6",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 6."
      },
      {
        "name": "Patient 7",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 7"
      },
      {
        "name": "Patient 8",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 8"
      },
      {
        "name": "Patient 9",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 9"
      },
      {
        "name": "Patient 10",
        "profilePic": "assets/img/speakers/avatar.jpg",
        "about": "Im a Patient 10"
      }
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
